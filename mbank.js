var maxResults = 5;

window.onload = function() {
  var list = browseTable('sfiStockTable', maxResults);
  appendBestInvestmentsToList(list);
};

function setMaxResults(results) {
  maxResults = results;
}

function onFailure(msg) {
  alert(msg);
}

var highlightBestInvestmentRates = function(element) {
  element.style.color = "red";
  element.style.fontWeight = "bold";
};

var appendElementToList = function(listClass, text, error) {
  var ulList = document.getElementsByClassName(listClass);
  if(ulList.length > 0) {
    alert(ulList.length);
    var ul = ulList[index];
    var li = document.createElement('li');
    li.appendChild(document.createTextNode(text));
    ul.appendChild(li);
  } else {
    error('Not found any aside-list.');
  }
};

var appendBestInvestmentsToList = function(fundsList) {
  var columnNarrowList = document.getElementsByClassName('column narrow');
  var columnNarrow = columnNarrowList[0];
  var asideId = 'bestFunds';
  var aside = document.getElementById(asideId);
  if(aside !== null){
    aside.innerHTML = "";
  } else {
    aside = document.createElement('aside');
    aside.setAttribute("id", asideId);
    columnNarrow.appendChild(aside);
  }
  var h4 = document.createElement('h4');
  h4.appendChild(document.createTextNode('Najlepsze fundusze'));
  h4.setAttribute('class', 'line-down');
  aside.appendChild(h4);
  var div = document.createElement('div');
  var ul = document.createElement('ul');
  ul.setAttribute('class', 'aside-list');
  if(fundsList !== undefined) {
    for(var i=0; i<fundsList.length; ++i) {
      var li = document.createElement('li');
      li.innerHTML = fundsList[i];
      ul.appendChild(li);
    }
  }
  div.appendChild(ul);
  aside.appendChild(div);
};

var browseTable = function(tableId, results) {
  var table = document.getElementById(tableId);

  var rowsCount = table.rows.length;
  if(results === undefined) {
    results = 5;
  }

  //alert(rowsCount);
  if(rowsCount) {
    var colsCount = table.rows[0].cells.length;
    function investmentFund(index, name, diff, ir) {
      this.index = index;
      this.name = name;
      this.difference = diff;
      this.interestRate = ir.slice(0);
    }
    var investmentFundIndex = {
      title: undefined,
      interestRate: []
    }
    var arrayOfInvestmentFund = [];
    // searching column titles
    for(var i=0; i<colsCount; ++i) {
      var cell = table.rows[0].cells[i];
      var isInterestRate  = cell.className.match(/\bdStopa/g) || cell.className.match(/\bdZmiana/g);
      var isHidden = cell.className.match(/\bhidden/g);
      if(cell.className === 'dNazwa') {
        investmentFundIndex.title = i;
      }
      if(isInterestRate && !isHidden) {
        investmentFundIndex.interestRate.push(i);
      }
    }

    if(investmentFundIndex.interestRate.length === 2) {
      var rowsCount = table.rows.length;
      for(var i=0; i < rowsCount; ++i) {
        var row = table.rows[i];
        firstInterestRate = Number(row.cells[investmentFundIndex.interestRate[0]].textContent.trim().replace(/,/g, '.'));
        secondInterestRate = Number(row.cells[investmentFundIndex.interestRate[1]].textContent.trim().replace(/,/g, '.'));
        var diff = secondInterestRate - firstInterestRate;
        if(!isNaN(diff)) {
          var name = row.cells[investmentFundIndex.title].textContent;
          var ir = [firstInterestRate, secondInterestRate];
          //add to the list investment fund
          arrayOfInvestmentFund.push(new investmentFund(i, name, diff, ir));
        }
      }

      var foundedFundsLinks = [];

      //sorting all investemnts funds consider comapring criteria
      arrayOfInvestmentFund.sort(function(a,b){return b.difference-a.difference;});
      for(var i=0; i<results; ++i) {
        var cell = table.rows[arrayOfInvestmentFund[i].index].cells[investmentFundIndex.title];
        highlightBestInvestmentRates(cell);
        foundedFundsLinks.push(cell.innerHTML);
        //console.log("Investment No." + i + ": Index: " + arrayOfInvestmentFund[i].index + ": " + arrayOfInvestmentFund[i].name + " Diff: " + arrayOfInvestmentFund[i].difference);
      }
      return foundedFundsLinks;
    } else {
      console.log("Error: You must check two interest rate to compare. Found " + investmentFundIndex.interestRate.length);
    }
  } else {
    console.log("Error: Any row was not found");
  }

};